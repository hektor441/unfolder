# Unfolder
# v. 0.1
# 16.08.16
from sys import argv

## Internal variables

code = ""
indx = 0
tokn = ""

cur_line = 0

str_help = """.:::Unfolder Language :::.

Base := Expr | Asgn | Loop
Loop := Expr 'for' [label] 'in' [int] ',' [int]
Asgn := [label] '=' Expr 
Expr := Term | Expr 'where' Asgn
Term := [num] | [label]
"""

sym_eof = "%EOF%"

sym_red = '\033[91m'
sym_grn = '\033[92m'
sym_blu = '\033[94m'
sym_blk = '\033[0m'

has_error = False
has_debug = True

symbols = ["(", ")", "+", "-", "*", "/", "%", "^", ",", "=", ":"]

# simple variables
var_sim = {}
# recursive variables
var_rec = {} 

# unknown variables
unknowns = []


## Useful functions

def islabel(s):
    return s.isalnum() and not s.isdigit()

def add_unknown(x):
    if not x in unknowns:
        unknowns.append(x)

def add_unknowns(xs):
    for x in xs:
        add_unknown(x)

def replace(array, elem0, elem1):
    # replaces each instance of elem0 with elem1 in an array
    i = 0
    while i < len(array):
        if array[i] == elem0:
            if type(elem1) == list:
                array = array[:i] + elem1 + array[i+1:]
                i+= len(elem1)
            else:
                array[i] = elem1
        i+= 1
    return array


## Tokenization

def tokenize(s):
    # split a string s into tokens
    for c in symbols:
        s = s.replace(c, " " + c + " ")
    
    s = s.replace("  ", " ")
    s = s.split(" ")
    
    while "" in s:
        s.remove("")
    return s
    
## Parsing necessaire

def succ():
    global indx, tokn
    # advance to the next token if possible
    if indx + 1 < len(code):
        indx+= 1
        tokn = code[indx]
    else:
        tokn = sym_eof
    return tokn

def expc(x):
    if tokn == x:
        return succ()
    err_msg = "`{}' expected but `{}' found.".format(x, tokn)
    return error(err_msg)

## Actual Parsing

def parse():
    ret = []
    while True:
        
        if tokn == sym_eof:
            break
    return ret


## Input / Output

def display(msg, color=sym_blk):
    # display a message in the interpreter
    if type(msg) == list:
        for line in msg:
            display(cur_line, sym_blk)
    else:        
        prompt = ">> "
        if color == sym_red:   prompt = "!! "
        elif color == sym_blu: prompt = "?? "
        elif color == sym_grn: prompt = "** "
        print(prompt + color + msg + sym_blk)

def error(msg):
    # display an error message
    global has_error
    has_error = True
    display("Error at line: {}, token: `{}'.".format(cur_line, tokn), sym_red)
    display(msg, sym_red)
    
def debug(msg):
    if has_debug:
        display(msg, color=sym_blu)


if len(argv) > 1:
    display("File Mode not yet implemented", sym_red)
else:
    display("Unfolder interpreter")
    display("Version 0.1")
    
    
    while True:
        cur_line+= 1
        prompt = "{}> ".format(str(cur_line) + " "*(3 - len(str(cur_line))))
        unknowns = []
        has_error = False
        g = str(input(prompt))
        if g == "exit":
            display("Goodbye!")
            break
        elif len(g) > 0:
            code = tokenize(g)
            indx = 0
            tokn = code[0]
            
            g = parse()
            if not has_error:
                print(">>>", g)
