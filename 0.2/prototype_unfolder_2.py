

d = {
# varname   varbody    unresolved variables    optional base value
    'a' : [['b'], ['b'], ['1']],
    'b' : [['a', '+', 'b'], ['a', 'b'], ['1']],
    'c' : [['$', '+', 'c'], ['c'], ['0']],
    'd' : [['pi', '+', '1'], ['pi'], []],
    'pi': [['3.1415'], [], []]
}


def replace(expr, a, b):
    # replace each instance of a in expr with b
    new_expr = []
    for i in range(0, len(expr)):
        if expr[i] == a:
            if type(b) == list:
                new_expr+= b
            else:
                new_expr.append(b)
        else:
            new_expr.append(expr[i])
    return new_expr

def is_var(s):
    return s.isalnum() and not s.isdigit()
    

def unfold(expr, unknowns, variables, level):
    # expand an expression with certain unknowns
    
    # replace special counter variable with level 0 
    expr = replace(expr, "$", ["0"])    

    for current_level in range(0, level):
        # for each unresolved variable
        
        new_expr = []
        new_unknowns = []
        for i in range(0, len(expr)):
            if expr[i] == "$":
                # replace the special counter variable with the current level
                new_expr.append(str(current_level))

            elif is_var(expr[i]):
                var = expr[i]

                if var in variables:
                    # find the variable's body and unknowns
                    var_body, var_unknowns = variables[var][0], variables[var][1]
                    
                    # add precautionary parentheses to var_body
                    if len(var_body) > 1:
                        var_body = ["("] + var_body + [")"]
                    
                    # add the new body to the expression instead of the variable
                    new_expr+= var_body
                    
                    # update the current list of unknowns
                    for unknown in var_unknowns:
                        if not unknown in new_unknowns:
                            new_unknowns.append(unknown)
                else:
                    # it's a new unfoldable variable
                    new_expr.append(var)
                    # update the unknowns
                    new_unknowns.append(var)   
            else:
                # it's an operator maybe? Just append it to the new expression
                new_expr.append(expr[i])
        expr = new_expr
        unknowns = new_unknowns
        
    # return the new expression with the new unknowns
    return expr, unknowns

def evaluate(expr, unknowns, variables, level, interval):
    # evaluate an expression with certain unknowns
    local_vars = {"$":"0"}
    
    expr, unknowns = unfold(expr, unknowns, variables, level)
    
    print("Trying to evaluate: ", "".join(expr))
    
    for var in unknowns:
        # unfold each unknown and try to evaluate it
        new_expr, new_unknowns = unfold([var], [var], variables, interval)
        print("\nWith variable", var, "=", "".join(new_expr), "=", end=" ")
        # replace each new_var in new_expr with their base value
        for new_var in new_unknowns:
            if new_var in variables:
                base_value = variables[new_var][2]
                new_expr = replace(new_expr, new_var, base_value)
        new_expr = replace(new_expr, "$", ["0"])
    
        # insert the             
        try:
            val = str(eval("".join(new_expr)))
            print(val)
            local_vars[var] = val
        except:
            print("Could not evaluate", "".join(new_expr))
    
    for k, v in local_vars.items():
        
        expr = replace(expr, k, v)        
    
    try:
        val = str(eval("".join(expr)))
        return val
    except:
        print("Could not evaluate*", "".join(expr))
        return False
    


print(evaluate(d['a'][0], d['a'][1], d, 10, 1))
    
    
    