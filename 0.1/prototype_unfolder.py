# fully resolved vars
# name : body
res_vars = {
    "a" : ["a"],
    
}
# recursively defined variables
# name : [body, unknowns]
rec_vars = {
    "f0" : [["f1", "+", "f2"], ["f1", "f2"]],
    "f1" : [["f0"], ["f0"]],
    "f2" : [["f1"], ["f1"]]
}




def insert_var(expr, var_name, var_body, use_parentheses=False):
    # this function replaces every instance of var_name in expression
    # with var_body, with an option to add outer parentheses to it
    i = 0
    while i < len(expr):
        if expr[i] == var_name:        
            if use_parentheses:
                expr = expr[:i] + ["("] + var_body + [")"] + expr[i+1:]
            else:
                expr = expr[:i] + var_body + expr[i+1:]
            i+= len(var_body)
        i+= 1
    return expr



def unfold(expr, variables, rec_level=0, rec_limit=1):
    ##print(rec_level, "."*rec_level, "Unfolding expression: {}, with variables: {}".format("".join(expr), variables))
    for var_name in variables:
        # try to unfold each variable
        if var_name in rec_vars:
            subexpr_body, subexpr_vars = rec_vars[var_name]
            
            if rec_level < rec_limit:
                # unfold a recursive variable
                ##print(rec_level, "."*rec_level, "Unfolding a recursive variable:",var_name)
                subexpr_body = unfold(subexpr_body[:], subexpr_vars, rec_level+1, rec_limit)
                
                # replace the special variable with level number
                for i in range(0, len(subexpr_body)):
                    if subexpr_body[i] == "$i":
                        subexpr_body[i] = str(rec_level)
                ##print(rec_level, "."*rec_level, var_name, "has become:", "".join(subexpr_body))
            
            expr = insert_var(expr, var_name, subexpr_body, len(subexpr_body) != 1)
    
        elif var_name in res_vars:
            # resolved variables stay the same
            subexpr_body = res_vars[var_name]
            expr = insert_var(expr, var_name, subexpr_body, len(subexpr_body) != 1)
        
    return expr

def evaluate(expr, variables, bases, rec_interval=3, rec_level=0, rec_limit=1):
    
    ##print(rec_level, "-"*rec_level,"!","Evaluating expression: {}, with variables: {} and bases: {}".format("".join(expr), variables, bases))
    
    # assign an evaluated value to each variable
    for var_name in variables:
        if var_name in rec_vars:
            # recursive variables need to be unfolded first
            if rec_level < rec_limit:
                ##print(rec_level, "-"*rec_level,"!", "Evaluating a recursive variable:",var_name)
                # unfold the variable and then evaluate it immediately
                subexpr_body, subexpr_vars = rec_vars[var_name]
                subexpr_body = "".join(unfold(subexpr_body[:], subexpr_vars, rec_limit=rec_interval))
                for k, v in bases.items():
                    subexpr_body = subexpr_body.replace(k, v)
                val = str(eval(subexpr_body))
                ##print(rec_level, "-"*rec_level,"!", var_name, "has now become:",val)
                bases[var_name] = val
        # non recursive variables already have a base definition
    
    # unfold the original expression a little bit and
    # insert the new found base values in place of the variables
    expr = unfold(expr, variables, rec_limit=rec_interval)
    expr = "".join(expr)
    ##print(rec_level, "-"*rec_level,"!", "Final unfolding:",expr,end=" ")
    for k, v in bases.items():
        expr = expr.replace(k, v)
    val = str(eval(expr))
    ##print("has value:",val)
    return val
    

for i in range(0, 30):
    print(eval("".join(unfold(rec_vars["f0"][0], rec_vars["f0"][1], rec_limit=i)).replace("f0","0").replace("f1","1").replace("f2","1")))
#print(evaluate(rec_vars["f0"][0], rec_vars["f0"][1], {"f0":"0", "f1":"1", "f2":"1"}))