# Unfolder
# v. 0.1
# 16.08.16
from sys import argv

## Internal variables

code = ""
indx = 0
tokn = ""

cur_line = 0

str_help = """.:::Unfolder Language :::.

Base := Expr | Asgn | Loop
Loop := Expr 'for' [label] 'in' [int] ',' [int]
Asgn := [label] '=' Expr 
Expr := Term | Expr 'where' Asgn
Term := [num] | [label]
"""

sym_eof = "%EOF%"

sym_red = '\033[91m'
sym_grn = '\033[92m'
sym_blu = '\033[94m'
sym_blk = '\033[0m'

has_error = False
has_debug = True

symbols = ["(", ")", "+", "-", "*", "/", "%", "^", ",", "=", ":"]

# simple variables
var_sim = {}
# recursive variables
var_rec = {} 

# unknown variables
unknowns = []


## Useful functions

def islabel(s):
    return s.isalnum() and not s.isdigit()

def add_unknown(x):
    if not x in unknowns:
        unknowns.append(x)

def add_unknowns(xs):
    for x in xs:
        add_unknown(x)

def replace(array, elem0, elem1):
    i = 0
    while i < len(array):
        if array[i] == elem0:
            if type(elem1) == list:
                array = array[:i] + elem1 + array[i+1:]
                i+= len(elem1)
            else:
                array[i] = elem1
        i+= 1
    return array


## Tokenization

def tokenize(s):
    # split a string s into tokens
    for c in symbols:
        s = s.replace(c, " " + c + " ")
    
    s = s.replace("  ", " ")
    s = s.split(" ")
    
    while "" in s:
        s.remove("")
    return s
    
## Parsing necessaire

def succ():
    global indx, tokn
    # advance to the next token if possible
    if indx + 1 < len(code):
        indx+= 1
        tokn = code[indx]
    else:
        tokn = sym_eof
    return tokn

def expc(x):
    if tokn == x:
        return succ()
    err_msg = "`{}' expected but `{}' found.".format(x, tokn)
    return error(err_msg)

## Actual parsing

def parse():
    ret = []
    while True:
        if tokn in [sym_eof]: break
        ret = parse_expr()
        if has_error: return False
        
        if tokn == "=":
            if not islabel("".join(ret)):
                err_msg = "Left value `{}' of assignment is not a valid label."
                return error(err_msg.format("".join(ret)))
            succ()
            ret = parse_asgn("".join(ret))
            
    return ret

def parse_expr():
    ret = parse_subexpr()
    if has_error: return False
    
    debug_msg = "Current expression `{}', the unknowns are now ({})"
    debug(debug_msg.format("".join(ret), ", ".join(unknowns)))
     
    
    if tokn in ["where", ":"]:
        succ()
        ret = parse_where(ret)
        if has_error: return False
    
    debug_msg = "Current expression `{}', the unknowns are now ({})"
    debug(debug_msg.format("".join(ret), ", ".join(unknowns)))
    
    if len(unknowns) == 0 and not tokn in ["=", "for"]:
        try:
            ret = replace(ret, "^", "**")
            debug("Trying to evaluate expression `{}'".format("".join(ret)))
            ret = str(eval("".join(ret)))
        except:
            return error("Could not evaluate the expression `{}'".format("".join(ret)))
    return ret
    
def parse_subexpr():
    ret = parse_term()
    if has_error: return False # guard
    
    #debug_msg = "Current sub-expression `{}', the unknowns are now ({})"
    #debug(debug_msg.format("".join(ret), ", ".join(unknowns)))
    print(ret)
        
    while tokn in ["+", "-", "*", "/", "%", "^"]:
        op  = tokn; succ()
        
        arg = parse_subexpr()
        if has_error: return False # guard
        
        ret+= [op] + arg
    
    return ret

def parse_term():
    ret = []
    if islabel(tokn):
        # tokn is a label
        debug_msg = "Label found `{}' ".format(tokn)
        ret = [tokn]; succ()
        if tokn == "=":
            # tokn is used in an assignment
            debug_msg+= "used later in an assignment."
            pass  
        elif ret[0] in var_sim:
            # tokn is a simple variable, substitute it on the fly
            ret = [var_sim[ret[0]]]
            debug_msg+= "is a simple variable of value `{}'.".format(ret[0])
        elif ret[0] in var_rec:
            # tokn is a recursive variable, substitute it on the fly
            name = ret[0]
            ret = var_rec[ret[0]]
            # and add its unresolved variables to the list
            add_unknowns(var_rec[name][1])
            debug_msg+= "is a recursive variable of value `{}' and unknowns ({})."\
                .format(ret[0], ", ".join(var_rec[name][1]))
        else:
            # it's an unresolved variable
            add_unknown(ret[0])
            debug_msg+= "is an unresolved variable (unknown)."
        debug(debug_msg)
        
    elif tokn.isdigit():
        # tokn is a number
        ret = [tokn]; succ()
    elif tokn == "(":
        succ()
        ret = parse_subexpr()
        if not expc(")"): # guard
            return False
    else:
        err_msg = "Unknown term `{}'"
        ret = [error(err_msg.format(tokn))]
    return ret

def parse_where(ret):
    debug("Parsing a where expression")
    while islabel(tokn):
        # parse "name '=' body" and replace "name" into ret
        name = tokn; succ()
        if not name in ret:
            err_msg = "The variable `{}' does not figure in expression `{}'"
            return error(err_msg.format(name, "".join(ret)))
        if not expc("="): # guard
            return False
        
        body = parse_subexpr()
        if not body: return False # guard
        
        
        unknowns.remove(name)
        ret = replace(ret, name, body)
        
        debug_msg = "`{}' replaced with `{}', the unknowns are now ({})"
        debug(debug_msg.format(name, "".join(body), ", ".join(unknowns)))
        
        # now there's either a for loop or a comma
        # followed by another variable assignment
        if tokn == "for": break
        elif not tokn == ",": break
        else: expc(",")
    return ret

def parse_asgn(name):
    global unknowns, var_sim, var_rec
    ret = ""
    
    debug("Parsing assignment")
    
    body = parse_expr()
    if not body: return False # guard
    
    debug_msg = "Definition for `{}' found, `{}' with unknowns ({})"
    debug(debug_msg.format(name, body, ", ".join(unknowns)))
    
    if len(unknowns) == 0:
        var_sim[name] = body
    else:
        var_rec[name] = [body, unknowns[:]]
    unknowns = []
    return True

def parse_loop():
    ret = ""
    return ret

## Input / Output

def display(msg, color=sym_blk):
    # display a message in the interpreter
    if type(msg) == list:
        for line in msg:
            display(cur_line, sym_blk)
    else:        
        prompt = ">> "
        if color == sym_red:   prompt = "!! "
        elif color == sym_blu: prompt = "?? "
        elif color == sym_grn: prompt = "** "
        print(prompt + color + msg + sym_blk)

def error(msg):
    # display an error message
    global has_error
    has_error = True
    display("Error at line: {}, token: `{}'.".format(cur_line, tokn), sym_red)
    display(msg, sym_red)
    
def debug(msg):
    if has_debug:
        display(msg, color=sym_blu)


if len(argv) > 1:
    display("File Mode not yet implemented", sym_red)
else:
    display("Unfolder interpreter")
    display("Version 0.1")
    
    
    while True:
        cur_line+= 1
        prompt = "{}> ".format(str(cur_line) + " "*(3 - len(str(cur_line))))
        unknowns = []
        has_error = False
        g = str(input(prompt))
        if g == "exit":
            display("Goodbye!")
            break
        elif len(g) > 0:
            code = tokenize(g)
            indx = 0
            tokn = code[0]
            
            g = parse()
            if not has_error:
                print(">>>", g)
        
    